export const utils = {
    /**
     *
     * @param classes - array of element classes
     * @return {HTMLDivElement}
     */
    createDiv(classes) {
        const div = document.createElement('div');
        div.classList.add(...classes);

        return div
    },
    /**
     *
     * @param text - an inner text
     * @return {HTMLAnchorElement}
     */
    createLink(text) {
        const a = document.createElement('a');
        a.innerText = text

        return a;
    },
    getStore() {
        return window['my-app'].store;
    }
}
