import {logo} from './Logo'
import {utils} from '../../services/utils';
import {pageLinks} from './page-links';
import {authorizationBlock} from "./authorization";

const headerElement = utils.createDiv(['header']);
headerElement.append(
    logo,
    pageLinks,
    authorizationBlock
);

export const header = headerElement
