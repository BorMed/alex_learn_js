import {utils} from '../../../services/utils';

const authorization = utils.createDiv(['header-authorization']);

const enterLinkWrapper = utils.createDiv('header-authorization-enter-link-wrapper')
const enterLink = utils.createLink('вход');
enterLinkWrapper.append(enterLink);

const registrationLinkWrapper = utils.createDiv('header-authorization-registration-link-wrapper')
const registrationLink = utils.createLink('регистрация');
registrationLinkWrapper.append(registrationLink);

authorization.append(enterLinkWrapper, registrationLinkWrapper);

export const authorizationBlock = authorization;
