import {utils} from "../../../services/utils";

const linksEl = utils.createDiv(['header-page-links']);
const pages = [
    {
        name: 'main',
        label: 'Главная'
    },
    {
        name: 'freight-car',
        label: 'Грузовые автомобили'
    },
    {
        name: 'cars',
        label: 'Легковые автомобили'
    }
]

pages.forEach((p) => {
    const link = utils.createDiv([`header-page-link-${p.name}`]);
    const a = document.createElement('a');
    a.innerText = p.label;
    link.append(a);
    linksEl.append(link);
    a.addEventListener(
        'click',
        () => {
            console.log('----------cars', utils.getStore().cars);
        }
    )
})

export const pageLinks = linksEl;
